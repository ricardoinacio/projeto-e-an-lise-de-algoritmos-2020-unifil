package br.unifil.dc.lab2.Algoritmos;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

public class Classificadores {

    final private static Random rngQs = new Random();

    public static void quicksort(List<Integer> lista) {
        quicksort(lista, (vs) -> vs.get(rngQs.nextInt(vs.size())));
    }
    public static void quicksort(List<Integer> lista, Function<List<Integer>, Integer> escolhedorPivo) {
        // Caso base
        if (lista.size() <= 1) return;

        // Casos de subdivisão recursiva
        Integer pivo = escolhedorPivo.apply(lista);
        int idxPivo = reorganizar(lista, pivo, (l, r) -> l.compareTo(r));

        quicksort(lista.subList(0, idxPivo));
        quicksort(lista.subList(idxPivo+1, lista.size()));
    }

    // Lomuto

    // Hoare
    private static int reorganizar(List<Integer> lista, Integer pivo, Comparator<Integer> cmp) {
        int l = 0, r = lista.size() - 1;
        while (true) {
            while (cmp.compare(lista.get(l), pivo) < 0) l++;
            while (cmp.compare(lista.get(r), pivo) > 0) r--;

            if (l < r) {
                permutar(lista, l, r);
            } else return l;
        }
    }

    /**
     * Classifica a lista em ordem crescente, pelo método de
     * mergesort, in-place. Utiliza memória auxiliar.
     * @param lista Lista a ser classificada, sofre mutação (in-place).
     *
     * Tempo de execução:
     * * Pior caso: 7n log n + 12 log n -> g(7n log n + 12 log n) -> O(n log n)
     */
    public static void mergesort(List<Integer> lista) {
        // Casos base
        if (lista.size() <= 1) return; // 1 * log n

        // Casos de subdivisão recursiva
        final int idxMeioLista = lista.size() / 2; // 1 * log n
        List<Integer> esquerda = lista.subList(0, idxMeioLista); // 1 * log n
        mergesort(esquerda); // 1 * log n

        List<Integer> direita = lista.subList(idxMeioLista, lista.size()); // 1 * log n
        mergesort(direita); // 1 * log n

        merge(lista, esquerda, direita); // (7n + 6) * log n
    } // 7n log n + 12 log n

    // Tempo de execução:
    // * Pior caso: 7n + 6
    private static void merge(List<Integer> lista, List<Integer> esquerda, List<Integer> direita) {
        List<Integer> merged = new ArrayList<>(lista); // 1

        int idxE = 0, idxD = 0, idxL = 0; // 1
        while (idxE < esquerda.size() && idxD < direita.size()) { // n
            if (esquerda.get(idxE) < direita.get(idxD)) { // n - 1
                merged.set(idxL, esquerda.get(idxE)); // n - 1
                idxE++; // n - 1
            } else {
                merged.set(idxL, direita.get(idxD));
                idxD++;
            }
            idxL++; // n - 1
        }

        int idxF;
        List<Integer> faltantes;
        if (idxE < esquerda.size()) { // 1
            faltantes = esquerda; // 1
            idxF = idxE; // 1
        } else {
            faltantes = direita;
            idxF = idxD;
        }

        while (idxF < faltantes.size()) { // 1 + 1
            merged.set(idxL, faltantes.get(idxF)); // 1
            idxL++; idxF++; // 1
        }

        for (int i = 0; i < lista.size(); i++) { // n + 1
            lista.set(i, merged.get(i)); // n
        }
    } // 7n + 6

    /**
     * Classifica a lista em ordem crescente, pelo método de
     * inserção, in-place.
     * @param lista Lista a ser classificada, sofre mutação (in-place).
     *
     * Desempenho: Ω(n) e O(n^2)
     */
    public static void insertionsort(List<Integer> lista) {
        //resetMedidores();

        for (int i = 1; i < lista.size(); i++) { // g(1 + n-1) -> g(n) -> Ω(n) | O(n) -> Θ(n)
            Integer elem = lista.get(i); // 1 * Ω(n) -> Ω(n) | O(n) -> Θ(n)

            int j = i; //comparacoes++; // Ω(n) | O(n) -> Θ(n)
            while (j > 0 && lista.get(j-1) > elem) { // Ω(n) | O(n) * O(n) -> O(n^2)
                //operacaoRwMemoria++;
                lista.set(j, lista.get(j-1)); // Deslocamento // O(n^2)

                j--; //comparacoes++; //  O(n^2)
            }

            lista.set(j, elem); // Ω(n) | O(n) -> Θ(n)
        }
    } // Ω(n) + Ω(n) + Ω(n) + Ω(n) + Ω(n) -> 5*Ω(n) -> Ω(5n) -> Ω(n) | 4*O(n) + 3*O(n^2) -> O(3n^2 + 4n) -> O(n^2)

    public static <Modelo extends Comparable<Modelo>> void bubblesortCrescente(List<Modelo> lista) {
        bubblesort(lista, (l, r) -> l.compareTo(r));
    }
    public static <Modelo extends Comparable<Modelo>> void bubblesortDeCrescente(List<Modelo> lista) {
        bubblesort(lista, (l, r) -> l.compareTo(r));
    }

    /**
     * Classifica a lista em ordem crescente, pelo método de
     * bubblesort, in-place.
     * @param lista Lista a ser classificada, sofre mutação (in-place).
     * @param comparador Função de comparação para correta ordenação.
     *
     * Desempenho:
     *  * Melhor caso: 1 + n + n - 1 + 1 = 2n + 1 -> Ω(n)
     *  * Pior caso:  5n^2 - 2n -> O(n^2)
     */
    public static <TElem> void bubblesort(List<TElem> lista, Comparator<TElem> comparador) {
        //resetMedidores();

        boolean houvePermuta; do {
            houvePermuta = false; // n

            // Sobe a bolha
            for (int i = 1; i < lista.size(); i++) { // n*n
                //comparacoes++;
                if (comparador.compare(lista.get(i - 1), lista.get(i)) > 0) { // n-1 (n)
                    permutar(lista, i - 1, i); // n-1 (n) * 3
                    houvePermuta = true; //n-1 (n)
                }
            }
        } while (houvePermuta); // n
    }

    /**
     * Classifica a lista em ordem crescente, pelo método de
     * selection sort, in-place.
     * @param lista Lista a ser classificada, sofre mutação (in-place).
     *
     * Desempenho:
     * * Melhor caso: n + 1 + 3n + 3n^2 + 3n = 2n^2 + 7n + 1 -> Ω(n^2)
     * * Pior caso: 3n^2 + 7n + 1 -> O(n^2)
     *
     * * Desempenho: Θ(n^2) : está em O(n^2) e Ω(n^2)
     */
    public static void selectionsort(List<Integer> lista) {
        resetMedidores();

        for (int i = 0; i < lista.size(); i++) { // n + 1
            int menorIdx = encontrarIndiceMenorElem(lista, i); // n(2n + 3)
            permutar(lista, menorIdx, i); // n * 3
        }
    }

    public static void bogosort(List<Integer> lista) {
        Random rng = new Random();
        while (!isOrdenada(lista)) {
            int a = rng.nextInt(lista.size());
            int b = rng.nextInt(lista.size());
            permutar(lista, a, b);
        }
    }

    public static String prettyPrintMedicoes() {
        return "Houve " + comparacoes + " comparacoes, e " + operacaoRwMemoria + " operacoes RW em memoria.";
    }


    /********
     *  SECAO DOS PRIVATES
     */

    private static boolean isOrdenada(List<Integer> lista) {
        for (int i = 1; i < lista.size(); i++)
            if (lista.get(i-1) > lista.get(i))
                return false;

        return true;
    }

    /**
     * Encontra o índice do menor elemento da lista.
     * @param lista lista a ser procurada.
     * @return índice do elemento, na escala iniciada em zero.
     *
     * Desempenho
     * * Melhor caso: 3
     * * Pior caso: 3n + 3
     */
    private static int encontrarIndiceMenorElem(List<Integer> lista, int idxInicio) {
        int menor = idxInicio; // 1
        for (int i = idxInicio+1; i < lista.size(); i++) { // n + 1
            //comparacoes++;
            if (lista.get(menor) > lista.get(i)) // n
                menor = i;
        }
        return menor; // 1
    }

    /**
     * Permuta (swap) dois elementos da lista de posição.
     * @param lista Lista cujos elementos serão permutados.
     * @param a Îndice do primeiro elemento a ser permutado.
     * @param b Îndice do outro elemento a ser permutado.
     *
     * Desempenho: 3, seja no melhor ou pior caso
     */
    private static <TElem> void permutar(List<TElem> lista, int a, int b) {
        TElem permutador = lista.get(a); // permutador = lista[a]
        lista.set(a, lista.get(b)); // lista[a] = lista[b]
        lista.set(b, permutador); // lista[b] = permutador

        //operacaoRwMemoria+=2;
    }

    private static void resetMedidores() {
        operacaoRwMemoria = 0;
        comparacoes = 0;
    }

    // NUNCA FACA ISSO PROFISSIONALMENTE!!!
    private static int operacaoRwMemoria = 0;
    private static int comparacoes = 0;
}
