package br.unifil.dc.lab2.Benchmarking;

import br.unifil.dc.lab2.Uteis.Aleatorios;
import br.unifil.dc.lab2.Algoritmos.Classificadores;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Benchmarking {

    public static void benchmarkHelperInsertionSort() {
        List<Integer> medido = Aleatorios.gerarListaAleatoria("benchmark".hashCode(),2000);
        System.out.println("Insertion sort levou " +
                benchmarkClassificadores(Classificadores::insertionsort, medido, 100)+ " s.");
    }

    public static double benchmarkClassificadores(
            Consumer<List<Integer>> classificador, List<Integer> medido, int repeticoes) {

        double tempoPassado = Double.POSITIVE_INFINITY;
        for (int i = 0; i < repeticoes; i++) {
            List<Integer> copia = new ArrayList<>(medido);

            // Horas antes
            long tempoAntes = System.nanoTime();
            classificador.accept(copia);
            // Horas depois
            long tempoDepois = System.nanoTime();
            // Diferença entre antes e depois.
            tempoPassado = Math.min(tempoPassado, tempoDepois - tempoAntes);
        }

        return tempoPassado * 1e9;
    }
}
