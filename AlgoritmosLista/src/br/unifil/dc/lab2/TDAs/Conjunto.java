package br.unifil.dc.lab2.TDAs;

import java.util.Iterator;

public interface Conjunto<T> extends Colecao<T>, Iterable<T> {
    void incluir(T obj);
    boolean excluir(T obj);
    boolean contem(T obj);

    default Conjunto<T> unirCom(Conjunto<T> outro) {
        Conjunto<T> uniao = novoConjuntoVazio();
        for (T elem : this)  uniao.incluir(elem);
        for (T elem : outro) uniao.incluir(elem);
        return uniao;
    }

    default Conjunto<T> intersecaoCom(Conjunto<T> outro) {
        Conjunto<T> intersecao = novoConjuntoVazio();
        for (T elem : this)
            if (outro.contem(elem))
                intersecao.incluir(elem);

        return intersecao;
    }

    default Conjunto<T> diferencaPara(Conjunto<T> subtraendo) {
        Conjunto<T> diferenca = novoConjuntoVazio();
        for (T elem : this)
            if (!subtraendo.contem(elem))
                diferenca.incluir(elem);

        return diferenca;
    }

    Conjunto<T> novoConjuntoVazio();

    // Exemplo de uso de iterador primitivamente, sem foreach
    /*default Conjunto<T> unirComPrimitivo(Conjunto<T> outro) {
        Conjunto<T> uniao = novoConjuntoVazio();
        //for (T elem : this)  uniao.incluir(elem);
        Iterator<T> iter = this.iterator();
        while (iter.hasNext()) {
            T elem = iter.next();
            uniao.incluir(elem);
        }

        for (T elem : outro) uniao.incluir(elem);
        iter = outro.iterator();
        while (iter.hasNext()) {
            T elem = iter.next();
            uniao.incluir(elem);
        }

        return uniao;
    }*/
}
