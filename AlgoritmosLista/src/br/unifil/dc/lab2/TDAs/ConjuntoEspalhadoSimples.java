package br.unifil.dc.lab2.TDAs;

import java.util.Iterator;

public class ConjuntoEspalhadoSimples<T> implements Conjunto<T> {

    @SuppressWarnings("unchecked")
    public ConjuntoEspalhadoSimples(int capacidade) {
        conjunto = (T[]) new Object[capacidade];
    }
    public ConjuntoEspalhadoSimples() {
        this(CAPACIDADE_PADRAO);
    }

    /**
     * Theta(1)
     * @param obj
     */
    @Override
    public void incluir(T obj) {
        int idx = getHashConjunto(obj);
        if (conjunto[idx] == null) qtdeElems++;
        conjunto[idx] = obj;
    }

    @Override
    public boolean excluir(T obj) {
        int idx = getHashConjunto(obj);
        if (obj.equals(conjunto[idx])){
            conjunto[idx] = null;
            qtdeElems--;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean contem(T obj) {
        return obj.equals(conjunto[getHashConjunto(obj)]);
    }

    @Override
    public Conjunto<T> novoConjuntoVazio() {
        return new ConjuntoEspalhadoSimples<>();
    }

    @Override
    public int qtdeElems() {
        return qtdeElems;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }


    private int getHashConjunto(T obj) {
        return obj.hashCode() % conjunto.length;
    }

    private T[] conjunto;
    private int qtdeElems = 0;
    private static final int CAPACIDADE_PADRAO = 8;
}
