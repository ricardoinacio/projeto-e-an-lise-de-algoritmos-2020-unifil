package br.unifil.dc.lab2.TDAs;

import java.util.Optional;

public interface Mapa<K,V> extends Colecao<V> {
    Optional<V> obter(K chave);
    V obterOuPadrao(K chave, V valor);

    Optional<V> associar(K chave, V valor);
    boolean associarSeAusente(K chave, V valor);
    Optional<V> trocar(K chave, V valor);

    boolean existe(K chave);
    boolean contem(V valor);

    Conjunto<K> chaves();
    Lista<V> valores();
}
