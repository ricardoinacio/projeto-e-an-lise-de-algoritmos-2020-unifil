package br.unifil.dc.lab2.TDAs;

public interface Colecao<T> {
    /**
     *
     * @return
     */
    int qtdeElems();

    /**
     *
     * @return
     */
    default boolean isVazia() {
        return qtdeElems() == 0;
    }
}
