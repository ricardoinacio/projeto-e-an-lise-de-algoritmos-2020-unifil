package br.unifil.dc.lab2.Testes;

import java.util.Arrays;

import static br.unifil.dc.lab2.Algoritmos.Recursividade.*;

public class RecursividadeTests {

    public static void testaExisteRecursivo() {
        int[] valores = { 1,2,3,4,5 };
        System.out.println("Espero true, tive " + existeRecursivo(3,valores));
        System.out.println("Espero true, tive " + existeRecursivo(5,valores));
        System.out.println("Espero false, tive " + existeRecursivo(0,valores));

        System.out.println("Espero true, tive " + existe(3,valores));
        System.out.println("Espero true, tive " + existe(5,valores));
        System.out.println("Espero false, tive " + existe(0,valores));
    }

    private static void testeFatorial() {
        System.out.println("Espero 24, tive " + fatorial(4));
    }

    private static void testeSomatorioLista() {
        System.out.println("Queria 10, tive " + somatorio(Arrays.asList(0, 1, 2, 3, 4)));
    }

    private static void testeFibonacci() {
        System.out.println("Esperava 1, e tive: " + fibRecursivo(1));
        System.out.println("Esperava 1, e tive: " + fibRecursivo(2));
        System.out.println("Esperava 55, e tive: " + fibRecursivo(10));

        for (int i = 1; i <= 50; i++) {
            System.out.println(i + ", " + fibonacci(i));
        }
    }
}
