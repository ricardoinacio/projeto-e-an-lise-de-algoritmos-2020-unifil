package br.unifil.dc.lab2.Conteineres;

public class Par<T, U> {
    public Par(T esquerda, U direita) {
        this.esquerda = esquerda;
        this.direita = direita;
    }

    public T getEsquerda() {
        return esquerda;
    }

    public U getDireita() {
        return direita;
    }

    @Override
    public String toString() {
        return "Par("+getEsquerda()+","+getDireita()+")";
    }

    private T esquerda;
    private U direita;
}